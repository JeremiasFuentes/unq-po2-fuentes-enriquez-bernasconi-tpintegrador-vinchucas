package proyecto.muestras;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import java.util.ArrayList;

class ZonaDeCoberturaTestCase {
	private ZonaDeCobertura unaZona, otraZona;
	private Ubicacion centro, otraUbicacion;
	private Muestra unaMuestra,otraMuestra;
	private Organizacion organizacion;
	private Sistema sistema;
	
	@BeforeEach
	void setUp() {
		centro = mock(Ubicacion.class);
		otraUbicacion = mock(Ubicacion.class);
		unaMuestra = mock (Muestra.class);
		otraMuestra = mock (Muestra.class);
		organizacion = mock (Organizacion.class);
		sistema = mock (Sistema.class);
		unaZona = new ZonaDeCobertura (centro,100.0,"Quilmes", sistema);
		otraZona = new ZonaDeCobertura (otraUbicacion,1.0,"Villa Palito", sistema);
	}
	
	@Test
	void testConstructor() {
		assertFalse(unaZona == null);
		assertEquals("Quilmes", unaZona.getNombre());
		assertTrue(unaZona.getOrganizacionesSuscriptas().isEmpty());
		assertEquals(sistema, unaZona.getSistema());
		verify(sistema, atMost(1)).agregarZona(unaZona);
	}
	
	@Test
	void testUnaZonaSeSolapaConSiMisma() {
		when(centro.distanceTo(centro)).thenReturn(0.0) ;
		assertTrue(unaZona.seSolapaCon(unaZona));
	}
	
	@Test
	void testUnaZonaNoSeSolapaConOtraZona() {
		when(centro.distanceTo(otraUbicacion)).thenReturn(120.0) ;
		assertFalse(unaZona.seSolapaCon(otraZona));
	}
	
	@Test
	void testUnaZonaSeSolapaConOtraZona() {
		when(centro.distanceTo(otraUbicacion)).thenReturn(12.0) ;
		assertTrue(unaZona.seSolapaCon(otraZona));
	}
	
	@Test
	void testUnaZonaContieneASuCentro() {
		when(centro.distanceTo(centro)).thenReturn(0.0) ;
		assertTrue(unaZona.contieneAlPunto(centro));
	}
	
	@Test
	void testUnaZonaNoContieneAUnPunto() {
		when(centro.distanceTo(otraUbicacion)).thenReturn(10000.0);
		assertFalse(unaZona.contieneAlPunto(otraUbicacion));
	}
	
	@Test
	void testUnaZonaNoContieneAUnaMuestra() {
		when(unaMuestra.getUbicacion()).thenReturn(otraUbicacion);
		when(centro.distanceTo(otraUbicacion)).thenReturn(10000.0) ;
		assertFalse(unaZona.contieneAlPunto(otraUbicacion));
	}
	
	@Test
	void testUnaZonaContieneAUnaMuestra() {
		when(unaMuestra.getUbicacion()).thenReturn(otraUbicacion);
		when(centro.distanceTo(otraUbicacion)).thenReturn(10.0) ;
		assertTrue(unaZona.contieneAlPunto(otraUbicacion));
	}
	
	@Test
	void testLasMuestrasContenidasDeUnaListaVaciaEsVacia() {
		ArrayList<Muestra> spyListaDeMuestras = spy(new ArrayList<Muestra>()) ;
		assertEquals(0, (unaZona.contenidasEnZona(spyListaDeMuestras).size())  );
	}
	
	@Test
	void testLasMuestrasContenidasDeUnaListaDeDosEsUna() {
		ArrayList<Muestra> listaDeMuestras = new ArrayList<Muestra>() ;
		listaDeMuestras.add(unaMuestra);
		listaDeMuestras.add(otraMuestra);
		
		when(unaMuestra.getUbicacion()).thenReturn(centro);
		when(centro.distanceTo(centro)).thenReturn(0.0) ;
		
		when(otraMuestra.getUbicacion()).thenReturn(otraUbicacion);
		when(centro.distanceTo(otraUbicacion)).thenReturn(10000.0) ;
		
		assertEquals(1, (unaZona.contenidasEnZona(listaDeMuestras).size())  );
	}
	
	@Test
	void testSuscribirOrganizacion() {
		unaZona.suscribir(organizacion);
		assertEquals(1, unaZona.getOrganizacionesSuscriptas().size());
	}
	
	@Test
	void testDesuscribirOrganizacion() {
		unaZona.suscribir(organizacion);
		unaZona.desuscribir(organizacion);
		assertTrue(unaZona.getOrganizacionesSuscriptas().isEmpty());
	}
	
	@Test
	void testNotificarNuevaValidacion() {
		unaZona.suscribir(organizacion);
		unaZona.suscribir(organizacion);
		unaZona.suscribir(organizacion);
		unaZona.suscribir(organizacion);
		unaZona.notificarNuevaValidacion(unaMuestra);
		verify(organizacion, atLeast(1)).updateNuevaValidacion(unaZona, unaMuestra);
		verify(organizacion, atMost(4)).updateNuevaValidacion(unaZona, unaMuestra);
	}
	
	@Test
	void testNotificarNuevaMuestra() {
		unaZona.suscribir(organizacion);
		unaZona.suscribir(organizacion);
		unaZona.suscribir(organizacion);
		unaZona.suscribir(organizacion);
		unaZona.notificarNuevaMuestra(unaMuestra);
		verify(organizacion, atLeast(1)).updateNuevaMuestra(unaZona, unaMuestra);
		verify(organizacion, atMost(4)).updateNuevaMuestra(unaZona, unaMuestra);
	}
}
