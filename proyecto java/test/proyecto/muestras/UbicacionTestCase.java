package proyecto.muestras;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

class UbicacionTestCase {
	Ubicacion unaUbicacion,otraUbicacion;
	Muestra unaMuestra,otraMuestra;
	ArrayList<Ubicacion> listaUbicaciones= new ArrayList<Ubicacion>();
	ArrayList<Ubicacion> listaVacia= new ArrayList<Ubicacion>();
	ArrayList<Muestra> listaMuestras = new ArrayList<Muestra>();
	Sistema sistema = mock(Sistema.class);
	
	@BeforeEach
	void setUp() {
		unaUbicacion= new Ubicacion(10.0,20.0);
		otraUbicacion= new Ubicacion(10.1,20.0);
		unaMuestra = mock(Muestra.class);
		otraMuestra = mock(Muestra.class);
		listaMuestras.add(unaMuestra);
		listaMuestras.add(otraMuestra);
	}
	
	@Test
	void testGetLatitudYLongitud() {
		assertEquals(10.0, unaUbicacion.getLatitude());
		assertEquals(20.0, unaUbicacion.getLongitude());
	}	
	
	@Test
	void testLaDistanciaASiMismaEsCero() {
		assertEquals(0.0,unaUbicacion.distanceTo(unaUbicacion));
	}
	
	@Test
	void testListaSinUbicacionesDevuelveListaVacia() {
		assertEquals( listaVacia, unaUbicacion.masCercanasQue(1, listaVacia));
	}
	
	@Test
	void testDosUbicacionesEstanCercaAmbas() {
		listaUbicaciones.add(unaUbicacion);
		listaUbicaciones.add(otraUbicacion);
		assertEquals( listaUbicaciones, unaUbicacion.masCercanasQue(1200.0, listaUbicaciones));
	}
	
	@Test
	void testUnaUbicacionEstaMuyLejos() {
		listaUbicaciones.add(otraUbicacion);
		assertEquals( listaVacia, unaUbicacion.masCercanasQue(1.0, listaUbicaciones));
	}
	
	@Test
	void testDosMuestrasEstanCercaDeUnaUbicacion() {
		Muestra muestra = mock(Muestra.class);
		when(muestra.getUbicacion()).thenReturn(unaUbicacion);
		when(muestra.getSistema()).thenReturn(sistema);
		when(sistema.getMuestras()).thenReturn(listaMuestras);
		when(unaMuestra.getUbicacion()).thenReturn(unaUbicacion);
		
		when(unaMuestra.getUbicacion()).thenReturn(unaUbicacion);
		when(otraMuestra.getUbicacion()).thenReturn(otraUbicacion);
		assertEquals(2,unaUbicacion.obtenerMuestrasCercanas(1000.0, muestra).size());
	}
	
	@Test
	void testDosMuestrasEstanUnaCercaYUnaLejosDeUnaUbicacion() {
		Muestra muestra = mock(Muestra.class);
		when(muestra.getUbicacion()).thenReturn(unaUbicacion);
		when(muestra.getSistema()).thenReturn(sistema);
		when(sistema.getMuestras()).thenReturn(listaMuestras);
		
		when(unaMuestra.getUbicacion()).thenReturn(unaUbicacion);
		when(otraMuestra.getUbicacion()).thenReturn(otraUbicacion);
		assertEquals(1,unaUbicacion.obtenerMuestrasCercanas(1.0, muestra).size());
	}
}
