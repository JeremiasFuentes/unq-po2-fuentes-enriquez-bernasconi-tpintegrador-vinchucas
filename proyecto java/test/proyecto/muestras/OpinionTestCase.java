package proyecto.muestras;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

class OpinionTestCase {
	
	private OpinionDeInsecto insecto;
	private OpinionExperta opinionE;
	private OpinionBasica opinionB;
	private LocalDate fecha;
	private Verificacion verificacion;
	
	@BeforeEach
	public void setUp() {
		fecha = LocalDate.now();
		insecto = OpinionDeInsecto.VinchucaInfestans;
		opinionB = new OpinionBasica(insecto,fecha);
		opinionE = new OpinionExperta(insecto,fecha);
		verificacion = mock (Verificacion.class);
	}
	
	@Test
	void testElVeredictoDeUnaOpinionEsElNombreDelInsecto() {
		assertEquals("Vinchuca Infestans", opinionB.veredicto());
	}
	
	@Test
	void testUnaOpinionExpertaSabeQueEsDeExperto() {
		assertTrue(opinionE.esDeExperto());
	}
	
	@Test
	void testUnaOpinionBasicaSabeQueNoEsDeExperto() {
		assertFalse(opinionB.esDeExperto());
	}

	@Test 
	void testUnaOpinionDeHoyEsReciente() {
		assertTrue(opinionE.esReciente());	
	}
	
	@Test 
	void testUnaOpinionDeHaceMasDeUnMesNoEsReciente() {
		LocalDate fechaVieja = LocalDate.of(2020, 1, 1);
		Opinion newOp = new OpinionBasica(insecto, fechaVieja);
		assertFalse(newOp.esReciente());
	}
	
	@Test
	void testUnaOpinionExpertaNoCuentaSiElVeredictoNoCoincide() {
		assertEquals(0, opinionE.contarParaVerificacion("Chinche Foliada"));
	}
	
	@Test
	void testUnaOpinionExpertaCuentaSiElVeredictoCoincide() {
		assertEquals(1, opinionE.contarParaVerificacion(insecto.getNombreClasificacion()));
	}
	
	@Test 
	void testOpinionExpertaVotaEstadoExperto() {
		opinionE.opinarVotada(verificacion);
		verify(verificacion).agregarOpinionALista(opinionE);
		verify(verificacion).setFechaUltimaOpinion(opinionE.getFecha());
		//verify(verificacion).cambioDeEstadoVotadaAVotadaPorExperto();
	}

	@Test 
	void testOpinionBasicaVotaEstadoVotada() {
		opinionE.opinarVotadaExperto(verificacion);
		verify(verificacion).agregarOpinionALista(opinionE);
		verify(verificacion).setFechaUltimaOpinion(opinionE.getFecha());
	}
	
	/*
	@Test
	void testUnaVerificacionOpinadaPorUnExpertoNoPuedeSerOpinadaPorUnUsuarioBasico() throws Exception {
		when(op.esDeExperto()).thenReturn(true);
		when(op2.esDeExperto()).thenReturn(false);
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		when(op2.veredicto()).thenReturn("Imagen Poco Clara");
		//when(op2.verificarEstado()).thenReturn(new EstadoVotadaPorExperto());
		verificacion.agregarOpinionALista(op);
		
		Exception exception = assertThrows(Exception.class, () -> verificacion.addOpinion(op2));
		assertEquals("Esta muestra no puede ser opinada por un Usuario Basico", exception.getMessage());
	}
	
	@Test
	void testUnaVerificacionYaVerificadaNoPuedeSerVotada() throws Exception{
		when(op.esDeExperto()).thenReturn(true);
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		when(op2.esDeExperto()).thenReturn(true);
		when(op2.veredicto()).thenReturn("Vinchuca Infestans");
		verificacion.agregarOpinionALista(op);
		verificacion.agregarOpinionALista(op2);	
		Exception exception = assertThrows(Exception.class, () -> verificacion.addOpinion(op3));
		assertEquals("No es posible votar, Muestra ya verificada", exception.getMessage());
	}
	*/
}
