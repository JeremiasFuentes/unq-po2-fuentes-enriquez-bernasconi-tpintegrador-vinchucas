package proyecto.muestras;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SistemaTestCase {
	
	private Sistema sistema;
	private ZonaDeCobertura zona;
	private Muestra muestra;
	private Ubicacion ubicacion;
	
	@BeforeEach
	void setUp() throws Exception {
		sistema = new Sistema();
		zona = mock(ZonaDeCobertura.class);
		muestra = mock(Muestra.class);
		ubicacion = mock(Ubicacion.class);
		mock(Ubicacion.class);
	}

	@Test
	void testConstructor() {
		assertTrue(sistema.getZonasDeCobertura().isEmpty());
		assertTrue(sistema.getMuestras().isEmpty());
	}

	@Test
	void testAgregarZona() {
		sistema.agregarZona(zona);
		assertEquals(1, sistema.getZonasDeCobertura().size());
		assertEquals(zona, sistema.getZonasDeCobertura().get(0));
	}
	
	@Test
	void testSacarZona() {
		sistema.agregarZona(zona);
		sistema.sacarZona(zona);
		assertEquals(0, sistema.getZonasDeCobertura().size());
		assertTrue(sistema.getZonasDeCobertura().isEmpty());
	}
	
	@Test
	void testAgregarMuestra() {
		sistema.agregarMuestra(muestra);
		assertEquals(1, sistema.getMuestras().size());
		assertEquals(muestra, sistema.getMuestras().get(0));
	}
	
	@Test
	void testSacarMuestra() {
		sistema.agregarMuestra(muestra);
		sistema.sacarMuestra(muestra);
		assertEquals(0, sistema.getMuestras().size());
		assertTrue(sistema.getMuestras().isEmpty());
	}
	
	@Test
	void testNotGetNotifyCreacionMuestra() {
		sistema.agregarZona(zona);
		sistema.getNotifyCreacionMuestra(muestra);
		when(muestra.getUbicacion()).thenReturn(ubicacion);
		when(zona.contieneAlPunto(ubicacion)).thenReturn(false);
		verify(zona, never()).notificarNuevaMuestra(muestra);;
	}
	
	@Test
	void testGetNotifyCreacionMuestra() {
		sistema.agregarZona(zona);
		when(zona.contieneALaMuestra(muestra)).thenReturn(true);
		sistema.getNotifyCreacionMuestra(muestra);
		verify(zona, atLeast(1)).notificarNuevaMuestra(muestra);
	}
	
	@Test
	void testNotGetNotifyVerificacionMuestra() {
		sistema.agregarZona(zona);
		sistema.getNotifyVerificacionMuestra(muestra);
		when(muestra.getUbicacion()).thenReturn(ubicacion);
		when(zona.contieneAlPunto(ubicacion)).thenReturn(false);
		verify(zona, never()).notificarNuevaValidacion(muestra);
	}
	
	@Test
	void testGetNotifyVerificacionMuestra() {
		sistema.agregarZona(zona);
		when(zona.contieneALaMuestra(muestra)).thenReturn(true);
		sistema.getNotifyVerificacionMuestra(muestra);
		verify(zona, atLeast(1)).notificarNuevaValidacion(muestra);
	}
}
