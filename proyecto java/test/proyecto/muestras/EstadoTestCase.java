package proyecto.muestras;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EstadoTestCase {

	private EstadoVotada estadoV;
	private EstadoVotadaPorExperto estadoE;
	private List<Opinion> listaDeOpiniones;
	private Opinion op1;
	private Opinion op2;
	private Opinion op3;
	private Opinion op4;
	private Opinion op5;
	private Opinion op6;
	private HashMap<String, Integer> counter;
	
	
	@BeforeEach
	void setUp() throws Exception {
		estadoV = new EstadoVotada();
		estadoE = new EstadoVotadaPorExperto();
		op1 = mock(Opinion.class);
		op2 = mock(Opinion.class);
		op3 = mock(Opinion.class);
		op4 = mock(Opinion.class);
		op5 = mock(Opinion.class);
		op6 = mock(Opinion.class);
		listaDeOpiniones = new ArrayList<Opinion>(Arrays.asList(op1, op2, op3, op4, op5, op6));
		counter = new HashMap<String, Integer>();
	}

	@Test
	void testCountRepetitionsVotada() {
		when(op1.veredicto()).thenReturn("Vinchuca Infestans");
		when(op2.veredicto()).thenReturn("Vinchuca Guasayana");
		when(op3.veredicto()).thenReturn("Chinche Foliada");
		when(op4.veredicto()).thenReturn("Vinchuca Infestans");
		when(op5.veredicto()).thenReturn("Vinchuca Guasayana");
		when(op6.veredicto()).thenReturn("Vinchuca Infestans");
		counter = estadoV.countRepetitions(listaDeOpiniones);
		assertEquals(3, counter.get("Vinchuca Infestans"));
		assertEquals(2, counter.get("Vinchuca Guasayana"));
		assertEquals(1, counter.get("Chinche Foliada"));		
	}
	
/*	@Test
	void testCountRepetitionsVotadaPorExperto() {
		when(op1.veredicto()).thenReturn("Phtia-Chinche");
		when(op2.veredicto()).thenReturn("Imagen Poco Clara");
		when(op3.veredicto()).thenReturn("Vinchuca Sordida");
		when(op4.veredicto()).thenReturn("Phtia-Chinche");
		when(op5.veredicto()).thenReturn("Phtia-Chinche");
		when(op6.veredicto()).thenReturn("Vinchuca Sordida");
		when(op1.sumarAlContadorExperto()).thenReturn(1);
		when(op2.sumarAlContadorExperto()).thenReturn(1);
		when(op3.sumarAlContadorExperto()).thenReturn(1);
		when(op4.sumarAlContadorExperto()).thenReturn(1);
		when(op5.sumarAlContadorExperto()).thenReturn(0);
		when(op6.sumarAlContadorExperto()).thenReturn(0);
		counter = estadoE.countRepetitions(listaDeOpiniones);
		verify(op1).contarVotoExperto(counter);
		verify(op2).contarVotoExperto(counter);
		verify(op3).contarVotoExperto(counter);
		verify(op4).contarVotoExperto(counter);
		verify(op5).contarVotoExperto(counter);
		verify(op6).contarVotoExperto(counter);	
		}
*/
}
/*
VinchucaInfestans("Vinchuca Infestans"),
ImagenPocoClara("Imagen Poco Clara"),
ChincheFoliada("Chinche Foliada"),
VinchucaSordida("Vinchuca Sordida"),
VinchucaGuasayana("Vinchuca Guasayana"),
PhtiaChinche("Phtia-Chinche"),
Ninguna("Ninguna")*/