package proyecto.muestras;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class OpinionDeInsectoTestCase {

	private OpinionDeInsecto opDeInsecto;

	@Test
	void testUnaOpinionDeInsectoTieneUnNombre() {
		opDeInsecto = OpinionDeInsecto.VinchucaInfestans;
		assertEquals("Vinchuca Infestans" , opDeInsecto.getNombreClasificacion());
	}
	
	@Test
	void testUnaOpinionDeInsectoPuedeSerUnaChincheFoliada() {
		opDeInsecto = OpinionDeInsecto.ChincheFoliada;
		assertEquals("Chinche Foliada" , opDeInsecto.getNombreClasificacion());
	}
	
	@Test
	void testUnaOpinionDeInsectoPuedeSerUnaVinchucaSordida() {
		opDeInsecto = OpinionDeInsecto.VinchucaSordida;
		assertEquals("Vinchuca Sordida" , opDeInsecto.getNombreClasificacion());
	}
	
	@Test
	void testUnaOpinionDeInsectoPuedeSerUnaVinchucaGuasayana() {
		opDeInsecto = OpinionDeInsecto.VinchucaGuasayana;
		assertEquals("Vinchuca Guasayana" , opDeInsecto.getNombreClasificacion());
	}
	
	@Test
	void testUnaOpinionDeInsectoPuedeSerUnaVinchucaPhtiaChinche() {
		opDeInsecto = OpinionDeInsecto.PhtiaChinche;
		assertEquals("Phtia-Chinche" , opDeInsecto.getNombreClasificacion());
	}
	
	@Test
	void testUnaOpinionDeInsectoPuedeSerImagenPocoClara() {
		opDeInsecto = OpinionDeInsecto.ImagenPocoClara;
		assertEquals("Imagen Poco Clara" , opDeInsecto.getNombreClasificacion());
	}
	
	@Test
	void testUnaOpinionDeInsectoPuedeSerNinguna() {
		opDeInsecto = OpinionDeInsecto.Ninguna;
		assertEquals("Ninguna" , opDeInsecto.getNombreClasificacion());
	}

}
