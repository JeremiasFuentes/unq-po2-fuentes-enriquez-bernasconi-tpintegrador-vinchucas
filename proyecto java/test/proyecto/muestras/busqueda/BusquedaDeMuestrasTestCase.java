package proyecto.muestras.busqueda;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import proyecto.muestras.EstadoDeVerificacion;
import proyecto.muestras.EstadoVerificada;
import proyecto.muestras.Muestra;
import proyecto.muestras.OpinionDeInsecto;

import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.ArrayList;

class BusquedaDeMuestrasTestCase {

	private Muestra unaMuestra;
	private Muestra otraMuestra;
	private ArrayList<Muestra> listaDeMuestras;
	
	private CriterioAND criterioAnd;
	private CriterioOR criterioOr;
	private CriterioNivelDeValidacion criterioNivelValid;
	private CriterioFechaDeCreacionPosterior criterioCreacionPos;
	private CriterioFechaDeVerificacionPosterior criterioVerifPos;
	
	private CriterioFechaDeCreacionAnterior criterioCreacionAnt;
	private CriterioFechaDeVerificacionAnterior criterioVerifAnt;
	
	private CriterioTipoDeInsecto criterioTipo;
	
	private OpinionDeInsecto insecto;
	private LocalDate unaFecha;
	private EstadoDeVerificacion estadoAComparar;
	
	@BeforeEach
	void setUp() {
		unaMuestra = mock(Muestra.class);
		otraMuestra = mock(Muestra.class);
		listaDeMuestras = new ArrayList<Muestra>();
		
		insecto = OpinionDeInsecto.ChincheFoliada;
		criterioTipo = new CriterioTipoDeInsecto(insecto);
		
		criterioCreacionPos = new CriterioFechaDeCreacionPosterior(unaFecha);
		criterioVerifPos = new CriterioFechaDeVerificacionPosterior(unaFecha);
		
		criterioCreacionAnt = new CriterioFechaDeCreacionAnterior(unaFecha);
		criterioVerifAnt = new CriterioFechaDeVerificacionAnterior(unaFecha);
		
		
		estadoAComparar = mock(EstadoVerificada.class);
		criterioNivelValid = new CriterioNivelDeValidacion(estadoAComparar);
		
		criterioAnd = new CriterioAND(criterioCreacionPos, criterioTipo);
		criterioOr = new CriterioOR(criterioCreacionPos, criterioTipo);
	}
	
	@Test
	void testFiltroDeUnaListaVaciaYObtengoListaVacia() {
		assertTrue(criterioCreacionPos.cumpleElCriterio(listaDeMuestras).isEmpty());
	}
	
	@Test
	void testFiltroPorCriterioCreacionPosteriorUnaListaDe2MuestrasUnaCumpleYLaOtraNo() {
		when(unaMuestra.fueCreadaDespuesDe(unaFecha)).thenReturn(true);
		when(otraMuestra.fueCreadaDespuesDe(unaFecha)).thenReturn(false);
		listaDeMuestras.add(unaMuestra);
		listaDeMuestras.add(otraMuestra);
		assertEquals(1, criterioCreacionPos.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroPorCriterioVerificacionPosteriorUnaListaDe2MuestrasUnaCumpleYLaOtraNo() {
		when(unaMuestra.fueOpinadaDespuesDe(unaFecha)).thenReturn(true);
		when(otraMuestra.fueOpinadaDespuesDe(unaFecha)).thenReturn(false);
		listaDeMuestras.add(unaMuestra);
		listaDeMuestras.add(otraMuestra);
		assertEquals(1, criterioVerifPos.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroPorCriterioCreacionAnteriorUnaListaDe2MuestrasUnaCumpleYLaOtraNo() {
		when(unaMuestra.fueCreadaDespuesDe(unaFecha)).thenReturn(true);
		when(otraMuestra.fueCreadaDespuesDe(unaFecha)).thenReturn(false);
		listaDeMuestras.add(unaMuestra);
		listaDeMuestras.add(otraMuestra);
		assertEquals(1, criterioCreacionAnt.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroPorCriterioVerificacionAnteriorUnaListaDe2MuestrasUnaCumpleYLaOtraNo() {
		when(unaMuestra.fueOpinadaDespuesDe(unaFecha)).thenReturn(true);
		when(otraMuestra.fueOpinadaDespuesDe(unaFecha)).thenReturn(false);
		listaDeMuestras.add(unaMuestra);
		listaDeMuestras.add(otraMuestra);
		assertEquals(1, criterioVerifAnt.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroPorCriterioNivelDeValidacionUnaListaDe2MuestrasUnaCumpleYLaOtraNo() {
		when(estadoAComparar.nivelDeValidacion()).thenReturn("Verificada");
		when(unaMuestra.getNivelVerificacion()).thenReturn("Verificada");
		when(otraMuestra.getNivelVerificacion()).thenReturn("Votada");
		
		listaDeMuestras.add(unaMuestra);
		listaDeMuestras.add(otraMuestra);
		assertEquals(1, criterioNivelValid.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroPorCriterioTipoDeInsectoUnaListaDe2MuestrasUnaCumpleYLaOtraNo() {
		
		when(unaMuestra.getTipoDeVinchuca()).thenReturn(OpinionDeInsecto.ChincheFoliada.getNombreClasificacion());
		when(otraMuestra.getTipoDeVinchuca()).thenReturn(OpinionDeInsecto.ImagenPocoClara.getNombreClasificacion());
		
		listaDeMuestras.add(unaMuestra);
		listaDeMuestras.add(otraMuestra);
		assertEquals(1, criterioTipo.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroCriterioANDUnaMuestraYNoCumpleUnoDeEllos() {
		
		when(unaMuestra.getTipoDeVinchuca()).thenReturn(OpinionDeInsecto.ChincheFoliada.getNombreClasificacion());
		when(unaMuestra.fueCreadaDespuesDe(unaFecha)).thenReturn(false);
		listaDeMuestras.add(unaMuestra);
		
		assertEquals(0, criterioAnd.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroCriterioANDUnaMuestraYCumpleAmbos() {
		
		when(unaMuestra.getTipoDeVinchuca()).thenReturn(OpinionDeInsecto.ChincheFoliada.getNombreClasificacion());
		when(unaMuestra.fueCreadaDespuesDe(unaFecha)).thenReturn(true);
		listaDeMuestras.add(unaMuestra);
		
		assertEquals(1, criterioAnd.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroCriterioORUnaMuestraYCumpleUnoDeEllos() {
		
		when(unaMuestra.getTipoDeVinchuca()).thenReturn(OpinionDeInsecto.ChincheFoliada.getNombreClasificacion());
		when(unaMuestra.fueCreadaDespuesDe(unaFecha)).thenReturn(false);
		listaDeMuestras.add(unaMuestra);
		
		assertEquals(1, criterioOr.cumpleElCriterio(listaDeMuestras).size());
	}
	
	@Test
	void testFiltroCriterioORUnaMuestraYNoCumpleNingunoDeEllos() {
		
		when(unaMuestra.getTipoDeVinchuca()).thenReturn(OpinionDeInsecto.ImagenPocoClara.getNombreClasificacion());
		when(unaMuestra.fueCreadaDespuesDe(unaFecha)).thenReturn(false);
		listaDeMuestras.add(unaMuestra);
		
		assertEquals(0, criterioOr.cumpleElCriterio(listaDeMuestras).size());
	}
}
