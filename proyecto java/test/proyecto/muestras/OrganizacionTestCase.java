package proyecto.muestras;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OrganizacionTestCase {
	
	private Organizacion organizacion;
	private TipoOrganizacion tipoO;
	private FuncionalidadExterna funcionalidad1;
	private ZonaDeCobertura zonaDeCobertura;
	private Muestra muestra;

	@BeforeEach
	void setUp(){
		tipoO = TipoOrganizacion.SALUD;
		organizacion = new Organizacion(tipoO, 15);
		funcionalidad1 = mock(FuncionalidadExterna.class);
		zonaDeCobertura = mock(ZonaDeCobertura.class);
		muestra = mock(Muestra.class);
	}
	
	@Test
	void testTipoOrganizacion() {
		
		TipoOrganizacion tipo0 = TipoOrganizacion.SALUD;
		TipoOrganizacion tipo1 = TipoOrganizacion.EDUCATIVA;
		TipoOrganizacion tipo2 = TipoOrganizacion.CULTURAL;
		TipoOrganizacion tipo3 = TipoOrganizacion.ASISTENCIA;

		assertEquals("Salud", tipo0.getTipoOrganizacion());
		assertEquals("Educativa", tipo1.getTipoOrganizacion());
		assertEquals("Cultural", tipo2.getTipoOrganizacion());
		assertEquals("Asistencia", tipo3.getTipoOrganizacion());
	}

	@Test
	void testConstructor() {
		assertEquals(tipoO, organizacion.getTipoOrganizacion());
		assertEquals(15, organizacion.getCantidadPersonas());
	}
	
	@Test
	void testUnaOrganizacionTieneUnaFuncionalidadParaUnaNuevaMuestra() {
		organizacion.setFuncionalidadNuevaMuestra(funcionalidad1);
		assertEquals(funcionalidad1, organizacion.getFuncionalidadNuevaMuestra());
	}
	@Test
	void testUnaOrganizacionTieneUnaFuncionalidadParaLaVerificacionDeUnaMuestra(){
		organizacion.setFuncionalidadNuevaValidacion(funcionalidad1);
		assertEquals(funcionalidad1, organizacion.getFuncionalidadNuevaValidacion());
	}

	@Test
	void testUnaOrganizacionCreaUnEventoAlRecibirUnaNotificacionDeNuevaMuestra() {
		organizacion.setFuncionalidadNuevaMuestra(funcionalidad1);
		organizacion.updateNuevaMuestra(zonaDeCobertura, muestra);
		verify(funcionalidad1).nuevoEvento(organizacion, zonaDeCobertura, muestra);
	}
	
	@Test
	void testUnaOrganizacionCreaUnEventoAlRecibirUnaNotificacionMuestraVerificada() {
		organizacion.setFuncionalidadNuevaValidacion(funcionalidad1);
		organizacion.updateNuevaValidacion(zonaDeCobertura, muestra);
		verify(funcionalidad1).nuevoEvento(organizacion, zonaDeCobertura, muestra);
	}

}
