package proyecto.muestras;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import java.awt.image.BufferedImage;
import java.time.LocalDate;

class MuestraTestCase {
	
	private Muestra muestra,muestraVieja;
	private BufferedImage foto;
	private Ubicacion ubicacion;
	private Usuario participante;
	private OpinionDeInsecto insecto;
	private OpinionBasica op;
	private LocalDate fecha;
	private Sistema sistema;
	private LocalDate fechaPrevia;
	private LocalDate fechaPosterior;
	
	@BeforeEach
	public void setUp() throws Exception {
		fecha = LocalDate.now();
		ubicacion = mock(Ubicacion.class);
		foto = mock(BufferedImage.class);
		participante = mock(Usuario.class);
		op = mock(OpinionBasica.class);
		sistema = mock(Sistema.class);
		insecto = OpinionDeInsecto.VinchucaInfestans;
		muestra = new Muestra(insecto, foto, ubicacion, participante, fecha, op, sistema);
		fechaPrevia = LocalDate.of(2000, 03, 28);
		fechaPosterior = LocalDate.of(2050, 03, 28);
		muestraVieja =new Muestra(insecto, foto, ubicacion, participante, fechaPrevia, op, sistema);
	}
	
	@Test
	void testUnaMuestraSabeDeQueTipoDeVinchucaEsLaFotografia() {
		assertEquals("Vinchuca Infestans", muestra.getTipoDeVinchuca());
	}
	
	@Test
	void testUnaMuestraSabeCualEsLaFotografiaTratada() {
		assertEquals(foto, muestra.getFotografia());
	}
	
	@Test
	void testUnaMuestraSabeLaUbicacionDeSiMisma() {
		assertEquals(ubicacion, muestra.getUbicacion());
	}
	
	@Test
	void testUnaMuestraSabeLaIdentificacionDeLaPersonaQueLaEnvio() {
		muestra.getId();
		verify(participante).id();
	}
	
	@Test
	void testUnaMuestraSabeSuNivelDeVerificacion() {
		assertEquals("Votada", muestra.getNivelVerificacion());
	}
	
	@Test
	void testUnaMuestraSabeQuienEsSuAutor() {
		assertEquals(participante, muestra.getAutor());
	}
	
	@Test
	void testBaseDeZonaGetsNotifyCreacion() throws Exception {
		verify(sistema, atMost(1)).getNotifyCreacionMuestra(muestra);
	}
	
	@Test
	void testUnaMuestraSabeLaFechaDeSuCreacion() {
		assertEquals(fecha, muestra.getFecha());
	}
	
	@Test
	void testUnaMuestraEsRecienteSiFueCreadaHaceMenosDe30Dias() {
		assertTrue(muestra.esReciente());
	}
	
	@Test
	void testUnaMuestraNoEsRecienteSiFueCreadaHaceMasDe30Dias() {
		assertFalse(muestraVieja.esReciente());
	}
	
	@Test
	void testUnaMuestraSabeSiFueCreadaDespuesDeCiertaFecha() {
		assertTrue(muestra.fueCreadaDespuesDe(fechaPrevia));
	}
	
	@Test
	void testUnaMuestraSabeSiNoFueCreadaDespuesDeCiertaFecha() {
		assertFalse(muestra.fueCreadaDespuesDe(fechaPosterior));
	}
	
	@Test
	void testNotificarSistemaDeVerificacion(){
		muestra.notifySistemaVerificacionMuestra();
		verify(muestra.getSistema()).getNotifyVerificacionMuestra(muestra);
	}
	
	/*
	
	@Test
	void testUnaMuestraSabeElResultadoDeSuVeredictoActual() throws Exception {
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		assertEquals("Vinchuca Infestans" , muestra.getResultadoActual());
	}
	
	@Test
	void testUnaMuestraPuedeAgregarUnaOpinion() throws Exception{
		muestra.agregarOpinion(op);
		assertEquals(2, muestra.getListaDeOpiniones().size());
	}

	@Test
	void testUnaMuestraSabeSiFueOpinadaDespuesDeCiertaFecha() throws Exception {
		when(op.getFecha()).thenReturn(fecha);
		when(op.esDeExperto()).thenReturn(true);
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		muestra.agregarOpinion(op);
		assertTrue(muestra.fueOpinadaDespuesDe(fechaPrevia));
	}
	
	@Test
	void testUnaMuestraSabeSiNoFueOpinadaDespuesDeCiertaFecha() throws Exception {
		when(op.getFecha()).thenReturn(fecha);
		when(op.esDeExperto()).thenReturn(true);
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		muestra.agregarOpinion(op);
		assertFalse(muestra.fueOpinadaDespuesDe(fechaPosterior));
	}
*/
}
