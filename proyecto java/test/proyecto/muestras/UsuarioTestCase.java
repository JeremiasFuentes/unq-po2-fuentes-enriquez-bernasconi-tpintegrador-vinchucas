package proyecto.muestras;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.awt.image.BufferedImage;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UsuarioTestCase {
	
	private Usuario participante;
	private Muestra muestra;
	private OpinionDeInsecto insect;
	private Usuario participanteExperto;
	private BufferedImage foto;
	private Ubicacion ubicacion;
	private LocalDate fecha;
	private Sistema sistema;
	
	@BeforeEach
	public void setUp() {
		participante = Usuario.UsuarioBasico("1234");
		participanteExperto = Usuario.UsuarioExpertoExternamenteValidado("1235");
		muestra = mock(Muestra.class);
		foto = mock(BufferedImage.class);
		ubicacion = mock (Ubicacion.class);
		sistema = mock (Sistema.class);
	}

	@Test
	void testUnParticipantePuedeVotarUnaMuestra() throws Exception {
		insect = OpinionDeInsecto.VinchucaGuasayana;
		fecha = LocalDate.now();
		participante.verificarMuestra(insect , muestra, fecha);
		assertEquals(1, participante.getVotosEnMuestras().size());
	}
	
	@Test
	void testUnUsuarioPuedeEnviarUnaMuestra() throws Exception {
		fecha = LocalDate.now();
		participante.enviarMuestra(muestra);
		assertEquals(1, participante.getMuestrasEnviadas().size());
	}
	
	@Test
	void testUnUsuarioBasicoSinHaberVotadoNadaNoEsExperto() {
		assertFalse(participante.esExperto());
	}
	
	@Test
	void testUsuarioCreaNuevaMuestra() throws Exception {
		insect = OpinionDeInsecto.VinchucaGuasayana;
		fecha = LocalDate.now();
		Muestra muestra = participante.nuevaMuestra(insect, foto, ubicacion, fecha, sistema);
		assertEquals(participante, muestra.getAutor());
		assertEquals(insect.getNombreClasificacion(), muestra.getTipoDeVinchuca());
		assertEquals(foto, muestra.getFotografia());
		assertEquals(ubicacion, muestra.getUbicacion());
		assertEquals(fecha, muestra.getFecha());
		assertEquals(sistema, muestra.getSistema());
	}
	
	@Test
	void testUnUsuarioBasicoQueRealizo10Enviosy20votacionesEsExperto() throws Exception {
		insect = OpinionDeInsecto.VinchucaGuasayana;
		when(muestra.esReciente()).thenReturn(true);
		fecha = LocalDate.now();
		for(int i=0; i<20 ; i++) {
			Muestra newMuestra = mock(Muestra.class);
			participante.verificarMuestra(insect, newMuestra ,fecha);
		}
		for(int j = 0; j<10 ; j++) {
			participante.enviarMuestra(muestra);
		}
		assertTrue(participante.esExperto());
	}
	
	@Test
	void testUnUsuarioExpertoDejaDeSerloSiNoParticipa() throws Exception {
		fecha = LocalDate.of(2018, 11, 1);
		for(int i=0; i<20 ; i++) {
			Muestra newMuestra = mock(Muestra.class);
			participante.verificarMuestra(insect, newMuestra,fecha);
		}		
		for(int j = 0; j<10 ; j++) {
			participante.enviarMuestra(muestra);
		}
		
		assertFalse(participante.esExperto());
	}
	
	@Test
	void testUsuarioConoceSuId() {
		assertEquals("1234",participante.id());
	}
	
	@Test
	void testUnUsuarioExpertoExternamenteSiempreEsExperto() throws Exception {
		fecha = LocalDate.of(2018, 11, 1);
		participanteExperto.verificarMuestra(insect, muestra, fecha);
		participanteExperto.enviarMuestra(muestra);
		assertTrue(participanteExperto.esExperto());
	}
	
	@Test
	void testUnUsuarioNoPuedeVotarUnaMuestraQueYaVoto() throws Exception {
		participanteExperto.verificarMuestra(insect, muestra,fecha);
		Exception exception = assertThrows(Exception.class, () -> participanteExperto.verificarMuestra(insect, muestra,fecha));
		assertEquals("No es posible votar esta Muestra", exception.getMessage());
	}
	
	@Test
	void testUnUsuarioNoPuedeVotarUnaMuestraQueElHayaEnviado() throws Exception {
		participanteExperto.enviarMuestra(muestra);
		when(muestra.getAutor()).thenReturn(participanteExperto);
		Exception exception = assertThrows(Exception.class, () -> participanteExperto.verificarMuestra(insect, muestra,fecha));
		assertEquals("No es posible votar esta Muestra", exception.getMessage());
	}
}
