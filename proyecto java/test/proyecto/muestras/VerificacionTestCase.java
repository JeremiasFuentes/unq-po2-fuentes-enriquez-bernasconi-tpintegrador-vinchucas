package proyecto.muestras;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class VerificacionTestCase {
	
	private Verificacion verificacion;
	private Opinion op;
	private Opinion op2;
	private Opinion op3;
	private Muestra muestra;
	private EstadoVotada estadoVotada;
	private EstadoVotadaPorExperto estadoVotadaE;
	private HashMap<String, Integer> counterMap;
	
	@BeforeEach
	public void setUp() {
		muestra = mock(Muestra.class);
		verificacion = new Verificacion(muestra);
		op = mock(OpinionBasica.class);
		op2 = mock(Opinion.class);
		op3 = mock(Opinion.class);
		estadoVotada = mock(EstadoVotada.class);
		estadoVotadaE = mock(EstadoVotadaPorExperto.class);
		counterMap = new HashMap<String, Integer>();
		counterMap.put("No Definida", 0);
	}
	
	@Test
	void testAgregarOpinion() throws Exception {
		verificacion.agregarOpinion(op);
		assertEquals(1, verificacion.listaDeOpiniones().size());		
	}
	
	@Test 
	void testVerificacionDelegaEnEstadoAgregarOpinion() throws Exception{
		verificacion.setEstado(estadoVotada);
		verificacion.addOpinion(op);
		verify(estadoVotada).addOpinion(op, verificacion);
	}
	
	@Test
	void testUnaVerificacionSabeLaFechaDeSuUltimaVotacion() {
		verificacion.setFechaUltimaOpinion(LocalDate.now());
		assertEquals(LocalDate.now(), verificacion.getFechaUltimaOpinion());
	}

	@Test
	void testUnaVerificacionConUnaOpinionEstaEsLaMasVotada() throws Exception {
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		verificacion.agregarOpinionALista(op);
		assertEquals("Vinchuca Infestans" , verificacion.getOpinionMasVotada());
	}
	
	@Test
	void testUnaVerificacionConDosOpinionesComunesDiferentesEsNoDefinida() throws Exception {
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		when(op2.veredicto()).thenReturn("Imagen Poco Clara");
		
		counterMap.put("Vinchuca Infestans", 1);
		when(estadoVotada.countRepetitions(verificacion.listaDeOpiniones())).thenReturn(counterMap);
		
		verificacion.agregarOpinionALista(op);
		verificacion.setEstado(estadoVotada);
		
		counterMap.put("Imagen Poco Clara", 1);
		when(estadoVotada.countRepetitions(verificacion.listaDeOpiniones())).thenReturn(counterMap);
		verificacion.agregarOpinionALista(op2);
		
		assertEquals("No Definida" , verificacion.getOpinionMasVotada());
	}
	
	@Test
	void testUnaVerificacionConOpinionDiferenteDeDosExpertosSuVeredictoEsNoDefinifida() throws Exception {
		//when(op2.esDeExperto()).thenReturn(true);
		when(op2.veredicto()).thenReturn("Vinchuca Infestans");
		//when(op3.esDeExperto()).thenReturn(true);
		when(op3.veredicto()).thenReturn("Chinche Foliada");
		
		counterMap.put("Vinchuca Infestans", 1);
		when(estadoVotadaE.countRepetitions(verificacion.listaDeOpiniones())).thenReturn(counterMap);
		
		verificacion.agregarOpinionALista(op2);
		verificacion.setEstado(estadoVotadaE);
		
		counterMap.put("Chinche Foliada", 1);
		when(estadoVotadaE.countRepetitions(verificacion.listaDeOpiniones())).thenReturn(counterMap);
		verificacion.agregarOpinionALista(op3);
		
		assertEquals("No Definida" , verificacion.getOpinionMasVotada());
	}

	@Test
	void testUnaVerificacionConUnaOpinionComunYUnaDeExpertoElVeredictoEsLaDelExperto() throws Exception {
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		when(op2.veredicto()).thenReturn("Chinche Foliada");
		
		verificacion.agregarOpinionALista(op);

		counterMap.put("Chinche Foliada", 1);
		when(estadoVotadaE.countRepetitions(verificacion.listaDeOpiniones())).thenReturn(counterMap);
		
		verificacion.cambioDeEstadoVotadaAVotadaPorExperto(estadoVotadaE);
		verificacion.agregarOpinionALista(op2);
		
		assertEquals("Chinche Foliada" , verificacion.getOpinionMasVotada());
	}
	
	@Test
	void testMuestraVerificada() throws Exception {
		when(op.veredicto()).thenReturn("Chinche Foliada");
		when(op2.veredicto()).thenReturn("Vinchuca Infestans");
		when(op3.veredicto()).thenReturn("Vinchuca Infestans");
		
		verificacion.agregarOpinionALista(op);
		
		counterMap.put("Vinchuca Infestans", 1);
		when(estadoVotadaE.countRepetitions(verificacion.listaDeOpiniones())).thenReturn(counterMap);
		
		verificacion.cambioDeEstadoVotadaAVotadaPorExperto(estadoVotadaE);
		verificacion.agregarOpinionALista(op2);
		
		counterMap.replace("Vinchuca Infestans", 2);
		when(estadoVotadaE.countRepetitions(verificacion.listaDeOpiniones())).thenReturn(counterMap);
		verificacion.agregarOpinionALista(op3);
		
		assertEquals("Vinchuca Infestans" , verificacion.getOpinionMasVotada());
	}
	
	
	@Test
	void testUnaVerificacionConOpinionesDiferentesElVeredictoEsLaMasVotada() throws Exception {
		when(op.veredicto()).thenReturn("Vinchuca Infestans");
		when(op2.veredicto()).thenReturn("Vinchuca Infestans");	
		when(op3.veredicto()).thenReturn("Imagen Poco Clara");
		when(op.esDeExperto()).thenReturn(false);
		when(op2.esDeExperto()).thenReturn(false);
		when(op3.esDeExperto()).thenReturn(false);
		verificacion.agregarOpinionALista(op);
		verificacion.agregarOpinionALista(op2);
		verificacion.agregarOpinionALista(op3);
		assertEquals("Vinchuca Infestans" , verificacion.getOpinionMasVotada());
	}

	@Test
	void testUnaVerificacionConOpinionEnComunDeDosExpertosSuNivelDeValidacioneEsVerificada() throws Exception {
		when(op2.esDeExperto()).thenReturn(true);
		when(op2.veredicto()).thenReturn("Vinchuca Infestans");
		when(op2.contarParaVerificacion("Vinchuca Infestans")).thenReturn(1);
		when(op3.esDeExperto()).thenReturn(true);
		when(op3.veredicto()).thenReturn("Vinchuca Infestans");
		when(op3.contarParaVerificacion("Vinchuca Infestans")).thenReturn(1);
		verificacion.agregarOpinionALista(op2);
		verificacion.agregarOpinionALista(op3);
		verificacion.comprobarSiEstaVerificada();
		assertEquals("Verificada" , verificacion.nivelDeValidacion());
	}
	
	@Test
	void testUnaVerificacionConOpinionDiferenteDeDosExpertosSuNivelDeValidacioneEsVotada() throws Exception {
		when(op2.esDeExperto()).thenReturn(true);
		when(op2.veredicto()).thenReturn("Vinchuca Infestans");
		verificacion.agregarOpinionALista(op2);
		when(op3.esDeExperto()).thenReturn(true);
		when(op3.veredicto()).thenReturn("Chinche Foliada");
		verificacion.agregarOpinionALista(op3);
		assertEquals("Votada" , verificacion.nivelDeValidacion());
	}
	
	@Test 
	void testMuestraVotadaPorExpertoEsVotada() throws Exception{
		verificacion.cambioDeEstadoVotadaAVotadaPorExperto(estadoVotadaE);
		verificacion.addOpinion(op2);
		verify(estadoVotadaE).addOpinion(op2, verificacion);
	}
	
	@Test
	void testMuestraVerificada2() throws Exception {
		when(op.esDeExperto()).thenReturn(true);
		when(op.veredicto()).thenReturn("Chinche Foliada");
		when(op2.esDeExperto()).thenReturn(true);
		when(op2.veredicto()).thenReturn("Chinche Foliada");
	
		verificacion.agregarOpinionALista(op);
		verificacion.agregarOpinionALista(op2);
		assertEquals("Chinche Foliada" , verificacion.getOpinionMasVotada());
	}

	@Test
	void testBaseDeZonaGetsNotifyVerificacion() throws Exception {
		when(op.veredicto()).thenReturn("Vinchuca Sordida");
		when(op2.veredicto()).thenReturn("Vinchuca Sordida");
		
		verificacion.agregarOpinionALista(op);
		verificacion.agregarOpinionALista(op2);
		
		when(op.contarParaVerificacion("Vinchuca Sordida")).thenReturn(1);
		when(op2.contarParaVerificacion("Vinchuca Sordida")).thenReturn(1);
		
		verificacion.comprobarSiEstaVerificada();
		verify(muestra, atLeast(1)).notifySistemaVerificacionMuestra();
	}
}
