package proyecto.muestras;

import java.util.HashMap;

public class EstadoVotadaPorExperto extends EstadoDeVerificacion {
	
	public EstadoVotadaPorExperto() {
		this.nivelDeValidacion = NivelDeValidacion.Votada;
	}

	@Override
	protected void addOpinion(Opinion opinion, Verificacion verificacion) throws Exception {
		opinion.opinarVotadaExperto(verificacion);
	}
	
	@Override
	protected boolean esImportante(Opinion op) {
		return op.esDeExperto();
	}

	@Override
	protected int contarOpinion(Opinion op) {
		return op.sumarAlContadorExperto();
	}
	
	@Override
	protected void agregarAlContador(HashMap<String, Integer> counter, Opinion op) {
		op.contarVotoExperto(counter);
	}
}
