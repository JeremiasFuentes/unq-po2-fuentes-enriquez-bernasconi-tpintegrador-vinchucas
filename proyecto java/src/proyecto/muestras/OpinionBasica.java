package proyecto.muestras;

import java.time.LocalDate;
import java.util.HashMap;

public class OpinionBasica extends Opinion{

	public OpinionBasica(OpinionDeInsecto insecto, LocalDate fecha) {
		this.opinionDeInsecto = insecto;
		this.fecha = fecha;
	}
	
	@Override
	protected void opinarVotada(Verificacion verificacion) {
		verificacion.setEstado(new EstadoVotada());
		verificacion.agregarOpinionALista(this);
		verificacion.setFechaUltimaOpinion(this.getFecha());
	}
	
	@Override
	protected void opinarVotadaExperto(Verificacion verificacion) throws Exception {
		throw new Exception("Esta muestra no puede ser opinada por un Usuario Basico");
	}

	@Override
	protected int contarParaVerificacion(String veredicto) {
		return 0;
	}

	@Override
	protected Integer sumarAlContadorExperto() {
		return 0;
	}

	@Override
	protected void contarVotoExperto(HashMap<String, Integer> map) {}

	@Override
	public boolean esDeExperto() {
		// TODO Auto-generated method stub
		return false;
	}
}
