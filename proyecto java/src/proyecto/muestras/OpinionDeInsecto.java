package proyecto.muestras;

public enum OpinionDeInsecto {
	VinchucaInfestans("Vinchuca Infestans"),
	ImagenPocoClara("Imagen Poco Clara"),
	ChincheFoliada("Chinche Foliada"),
	VinchucaSordida("Vinchuca Sordida"),
	VinchucaGuasayana("Vinchuca Guasayana"),
	PhtiaChinche("Phtia-Chinche"),
	Ninguna("Ninguna");
	
	private String nombreClasificacion;
	
	OpinionDeInsecto(String nombre) {
		this.setNombreClasificacion(nombre);
	}
	
	public String getNombreClasificacion() {return nombreClasificacion;}

	public void setNombreClasificacion(String nombreClasificacion) {this.nombreClasificacion = nombreClasificacion;}

}
