package proyecto.muestras;

import java.util.HashMap;
import java.util.List;

public class EstadoNoVotada extends EstadoDeVerificacion {

	@Override
	protected void addOpinion(Opinion opinion, Verificacion verificacion) {
		opinion.opinarVotada(verificacion);
}

	@Override
	protected boolean esImportante(Opinion op) {
		return false;
	}

	@Override
	protected HashMap<String, Integer> countRepetitions(List<Opinion> votaciones) {
		HashMap<String, Integer> counter = new HashMap<String, Integer>();
		counter.put("No Definida", 0);
		String opinionInsecto = votaciones.get(0).veredicto();
		counter.put(opinionInsecto, 1);
		return counter;
	}

	@Override
	protected int contarOpinion(Opinion op) {
		return 0;
	}

	@Override
	protected void agregarAlContador(HashMap<String, Integer> counter, Opinion op) {
		// TODO Auto-generated method stub
		
	}
}
