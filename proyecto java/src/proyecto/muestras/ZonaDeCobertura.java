package proyecto.muestras;

import java.util.ArrayList;
import java.util.List;

public class ZonaDeCobertura {
	private String nombre;
	private Ubicacion epicentro;
	private double radio;
	private ArrayList<Organizacion> organizacionesSuscriptas;
	private Sistema sistema;
	
	public ZonaDeCobertura(Ubicacion epicentro, double radio, String nombre, Sistema sistema) {
		super();
		this.setNombre(nombre);
		this.setEpicentro(epicentro);
		this.setRadio(radio);
		this.organizacionesSuscriptas = new ArrayList<Organizacion>();
		this.setSistema(sistema);
	}
	public String getNombre() {
		return nombre;
	}
	private void setNombre(String nombre) {
		this.nombre = nombre;		
	}	
	private Ubicacion getEpicentro() {
		return epicentro;
	}
	private void setEpicentro(Ubicacion epicentro) {
		this.epicentro = epicentro;
	}
	private double getRadio() {
		return radio;
	}
	private void setRadio(double radio) {
		this.radio = radio;
	}
	
	public ArrayList<Organizacion> getOrganizacionesSuscriptas() {
		return organizacionesSuscriptas;
	}
		
	public Sistema getSistema() {
		return sistema;
	}
	public void setSistema(Sistema base) {
		this.sistema = base;
	}
	
	public boolean seSolapaCon(ZonaDeCobertura otraZona) {
		/* Retorna true si hay puntos en com�n entre esta zona y la que llega por par�metro
		 * y false si no los hay.
		 * Si la distancia entre sus centros es menor que la suma de sus radios,
		 * quiere decir que se solapan.
		 */
		double distanciaEntreCentros = this.getEpicentro().distanceTo(otraZona.getEpicentro());
		return (distanciaEntreCentros < (this.getRadio()+otraZona.getRadio()) );
	}
	
	public boolean contieneAlPunto(Ubicacion punto) {
		/* Si la distancia del punto al epicentro es menor que el radio de la zona
		 * entonces la zona contiene al punto.
		 */
		return (this.getEpicentro().distanceTo(punto) < this.getRadio());
	}
	
	public boolean contieneALaMuestra(Muestra unaMuestra) {
		return (this.contieneAlPunto(unaMuestra.getUbicacion()));
	}
	
	public List<Muestra> contenidasEnZona(List<Muestra> muestras){
		ArrayList<Muestra> muestrasContenidas = new ArrayList<Muestra>();
		for (Muestra muestra : muestras) {
			if (this.contieneALaMuestra(muestra)) {
				muestrasContenidas.add(muestra);
			}
		}
		return muestrasContenidas;
	}
	public void suscribir(Organizacion organizacion) {
		this.organizacionesSuscriptas.add(organizacion);
	}
	public void desuscribir(Organizacion organizacion) {
		this.organizacionesSuscriptas.remove(organizacion);		
	}
	public void notificarNuevaValidacion(Muestra muestra) {
		for(Organizacion o:this.getOrganizacionesSuscriptas()) {
			o.updateNuevaValidacion(this, muestra);
		}
	}
	public void notificarNuevaMuestra(Muestra muestra) {
		for(Organizacion o:this.getOrganizacionesSuscriptas()) {
			o.updateNuevaMuestra(this, muestra);
		}
	}

}
