package proyecto.muestras;

import java.time.LocalDate;

public class UsuarioExperto implements NivelDeUsuario{//Patron State: Concrete State
	@Override
	public Boolean esExperto() {
		return true;
	}

	@Override
	public Opinion nuevaOpinion(OpinionDeInsecto insecto, LocalDate fecha) {
		return (new OpinionExperta(insecto, fecha));
	}

}
