package proyecto.muestras;

import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class Muestra {

	private OpinionDeInsecto insectoFotografiado;
	private Verificacion verificacion = new Verificacion(this);
	private Ubicacion ubicacion;
	private BufferedImage foto;
	private LocalDate fecha;
	private Usuario autor;
	private Sistema sistema;

	public Muestra(OpinionDeInsecto insecto, BufferedImage foto, Ubicacion ubicacion, Usuario autor, LocalDate fecha, Opinion opinion, Sistema sistema) throws Exception {
		this.insectoFotografiado = insecto;
		this.ubicacion = ubicacion;
		this.autor = autor;
		this.foto = foto;
		this.fecha = fecha;
		this.agregarOpinion(opinion);
		this.sistema = sistema;
		this.notifySistemaCreacionMuestra(sistema);
	}

	private void notifySistemaCreacionMuestra(Sistema sistema) {
		sistema.getNotifyCreacionMuestra(this);
	}

	public void agregarOpinion(Opinion op) throws Exception {
		this.verificacion.addOpinion(op);
	}

	/*Codigo sin usar
	public void avisoDeVerificacion(String estadoPrevio, String estadoActual) {
		this.notifySistemaVerificacionMuestra();
	}*/
	
	public void notifySistemaVerificacionMuestra() {
		this.getSistema().getNotifyVerificacionMuestra(this);
	}
	
	public List<Opinion> getListaDeOpiniones() {
		return (this.verificacion.listaDeOpiniones());
	}
	
	public Boolean esReciente() {
		Long antiguedad = ChronoUnit.DAYS.between(this.getFecha(), LocalDate.now());
		return (antiguedad<=30);
	}

	public String getTipoDeVinchuca() {
		return insectoFotografiado.getNombreClasificacion();
	}

	public BufferedImage getFotografia() {
		return this.foto;
	}

	public Ubicacion getUbicacion() {
		return this.ubicacion;
	}

	public Object getId() {
		return this.autor.id();
	}
	
	public Usuario getAutor() {
		return this.autor;
	}

	public String getResultadoActual() {
		return verificacion.getOpinionMasVotada();
	}

	public String getNivelVerificacion() {
		return verificacion.nivelDeValidacion();
	}

	public LocalDate getFecha() {
		return this.fecha;
	}

	public boolean fueCreadaDespuesDe(LocalDate fechaAComparar) {
		return (this.getFecha().compareTo(fechaAComparar) >= 0);
	}

	public boolean fueOpinadaDespuesDe(LocalDate fechaAComparar) {
		return (verificacion.getFechaUltimaOpinion().compareTo(fechaAComparar) >= 0);
	}

	public Sistema getSistema() {
		return sistema;
	}
}
