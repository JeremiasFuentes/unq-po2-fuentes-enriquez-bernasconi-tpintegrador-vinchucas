package proyecto.muestras;

import java.util.HashMap;

public class EstadoVotada extends EstadoDeVerificacion{

	@Override
	protected void addOpinion(Opinion opinion, Verificacion verificacion) {
		opinion.opinarVotada(verificacion);
}

	@Override
	protected boolean esImportante(Opinion op) {
		return true;
	}

	@Override
	protected int contarOpinion(Opinion op) {
		return 1;
	}

	@Override
	protected void agregarAlContador(HashMap<String, Integer> counter, Opinion op) {
		counter.put(op.veredicto(), 1);
	}
}
