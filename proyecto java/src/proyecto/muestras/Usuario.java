package proyecto.muestras;

import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.util.ArrayList;

public class Usuario {
	
	private String id;
	private ArrayList<Muestra> muestrasVotadas;
	private ArrayList<Opinion> votosEnMuestras;
	private ArrayList<Muestra> muestrasEnviadas;
	private NivelDeUsuario nivel;
	private Boolean esExpertoValidadoExternamente;

	//Constructor custom
	public static Usuario UsuarioBasico(String id) {
		Usuario nuevoUsuario = new Usuario(id,false);
		return nuevoUsuario;
	}
	
	//Constructor custom
	public static Usuario UsuarioExpertoExternamenteValidado(String id) {
		Usuario nuevoUsuario = new Usuario(id,true);
		return nuevoUsuario;
	}
	
	//Este constructor es privado adrede, solo usar los de arriba
	private Usuario(String id, Boolean esExpertoValidadoExternamente) {
		super();
		this.id = id;
		this.esExpertoValidadoExternamente = esExpertoValidadoExternamente;
		this.muestrasEnviadas = new ArrayList<Muestra>();
		this.muestrasVotadas= new ArrayList<Muestra>();
		this.votosEnMuestras= new ArrayList<Opinion>();
		this.calcularNivel();
	}

	public String id() {
		return this.id;
	}

	public Opinion nuevaOpinion(OpinionDeInsecto insecto, LocalDate fecha) {
		return this.nivel.nuevaOpinion(insecto,fecha);
	}
	
	public Muestra nuevaMuestra(OpinionDeInsecto insecto, BufferedImage foto, Ubicacion ubicacion, LocalDate fecha, Sistema sistema) throws Exception {
		Muestra nuevaMuestra = new Muestra(insecto,foto,ubicacion,this,fecha,this.nuevaOpinion(insecto, fecha), sistema);
		return nuevaMuestra;
	}

	public void enviarMuestra(Muestra muestra) throws Exception {
		this.muestrasEnviadas.add(muestra);
		this.muestrasVotadas.add(muestra);
		this.calcularNivel();
	}

	private void calcularNivel() { //Patron State: Context
		if(this.esExpertoValidadoExternamente || this.condicionParaExperto()) {
			this.nivel = new UsuarioExperto();
		}
		else {
			this.nivel= new UsuarioComun();
		}
	}

	private boolean condicionParaExperto() {
		return (this.cantMuestrasDeLosUltimos30Dias() >= 10 &&
				this.cantVotosDeLosUltimos30Dias()	>= 20);
	}

	private int cantMuestrasDeLosUltimos30Dias() {
		int cuentaMuestrasRecientes=0;
		for (Muestra m : muestrasEnviadas) {
			if(m.esReciente()) {
				cuentaMuestrasRecientes++;
			}
		}
		return cuentaMuestrasRecientes;
	}
	
	private int cantVotosDeLosUltimos30Dias() {
		int cuentaVotosRecientes=0;
		for (Opinion op : this.votosEnMuestras) {
			if(op.esReciente()) {
				cuentaVotosRecientes++;
			}
		}
		return cuentaVotosRecientes;
	}
	
	public void verificarMuestra(OpinionDeInsecto insect, Muestra muestra, LocalDate fecha) throws Exception {
		if(this.puedevotarA(muestra)) {
			Opinion miOpinion = this.nuevaOpinion(insect,fecha);
			muestra.agregarOpinion(miOpinion);
			this.votosEnMuestras.add(miOpinion);
			this.muestrasVotadas.add(muestra);
			this.calcularNivel();
		}else {
			throw new Exception("No es posible votar esta Muestra");
		}
	}

	private boolean puedevotarA(Muestra muestra) {
		return !this.getMuestrasVotadas().contains(muestra) && muestra.getAutor() != this;
	}

	public ArrayList<Muestra> getMuestrasEnviadas() {
		return this.muestrasEnviadas;
	}
	
	public ArrayList<Opinion> getVotosEnMuestras() {
		return this.votosEnMuestras;
	}
	
	public ArrayList<Muestra> getMuestrasVotadas() {
		return this.muestrasVotadas;
	}
	
	public Boolean esExperto() {
		return this.nivel.esExperto();
	}

}
