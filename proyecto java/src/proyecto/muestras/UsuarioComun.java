package proyecto.muestras;

import java.time.LocalDate;

public class UsuarioComun implements NivelDeUsuario{ //Patron State: Concrete State
	@Override
	public Boolean esExperto() {
		return false;
	}

	@Override
	public Opinion nuevaOpinion(OpinionDeInsecto insecto, LocalDate fecha) {
		return (new OpinionBasica(insecto,fecha));
	}

}
