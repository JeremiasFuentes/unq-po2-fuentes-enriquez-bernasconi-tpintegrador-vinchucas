package proyecto.muestras.busqueda;

import java.util.ArrayList;
import java.util.List;

import proyecto.muestras.EstadoDeVerificacion;
import proyecto.muestras.Muestra;

public class CriterioNivelDeValidacion implements CriterioBusquedaMuestras {
	//Patron Composite: Leaf
	/*Para cumplir este criterio, las muestras deben estar validadas al mismo nivel que la var
	 * de instancia , que se pasa por parametro al crearla.
	 */
	private EstadoDeVerificacion estadoAComparar;

	public CriterioNivelDeValidacion(EstadoDeVerificacion estadoAComparar) {
		super();
		this.estadoAComparar = estadoAComparar;
	}

	@Override
	public List<Muestra> cumpleElCriterio(List<Muestra> listaDeMuestras) {	
		List <Muestra> muestrasQueCumplen = new ArrayList<Muestra>();
		for (Muestra m : listaDeMuestras) {
			if(m.getNivelVerificacion() == this.estadoAComparar.nivelDeValidacion()) {
				muestrasQueCumplen.add(m);
			}
		}
		return muestrasQueCumplen;
	}
	
	
}
/*
 * Dependencias: 
 * 				Muestra: getNiveDeVerificacion()
 * 				EstadoDeVerificacion: nivelDeValidacion()
 */