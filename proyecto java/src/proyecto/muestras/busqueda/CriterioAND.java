package proyecto.muestras.busqueda;

import java.util.List;

import proyecto.muestras.Muestra;
//Patron: Composite. (Composite)
public class CriterioAND implements CriterioBusquedaMuestras{
	private CriterioBusquedaMuestras unCriterio;
	private CriterioBusquedaMuestras otroCriterio;
	
	public CriterioAND(CriterioBusquedaMuestras unCriterio, CriterioBusquedaMuestras otroCriterio) {
		super();
		this.unCriterio = unCriterio;
		this.otroCriterio = otroCriterio;
	}

	@Override
	public List<Muestra> cumpleElCriterio(List<Muestra> listaDeMuestras) {
		//Genero lista que cumple el primer criterio, y retorno la lista que de esta ultima cumplen tambien el segundo
		List <Muestra> cumpleUno = unCriterio.cumpleElCriterio(listaDeMuestras);
		return otroCriterio.cumpleElCriterio(cumpleUno);
	}

}