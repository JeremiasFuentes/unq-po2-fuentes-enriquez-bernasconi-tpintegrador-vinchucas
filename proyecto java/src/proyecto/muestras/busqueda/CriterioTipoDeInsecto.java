package proyecto.muestras.busqueda;

import java.util.ArrayList;
import java.util.List;

import proyecto.muestras.Muestra;
import proyecto.muestras.OpinionDeInsecto;

public class CriterioTipoDeInsecto implements CriterioBusquedaMuestras{
	//Patron Composite: Leaf
	//Para cumplir este criterio, la tendencia actual sobre que insecto es debe ser el insecto de la var. de instancia
	private OpinionDeInsecto insecto;
	
	public CriterioTipoDeInsecto(OpinionDeInsecto insecto) {
		super();
		this.insecto = insecto;
	}	
	
	@Override
	public List<Muestra> cumpleElCriterio(List<Muestra> listaDeMuestras) {
		
		List <Muestra> muestrasQueCumplen = new ArrayList<Muestra>();
		for (Muestra m : listaDeMuestras) {
			if(m.getTipoDeVinchuca() == insecto.getNombreClasificacion()) {
				muestrasQueCumplen.add(m);
			}
		}
		return muestrasQueCumplen;
	}
}

/*
 * Dependencias: 
 * 				Muestra: getTipoDeVinchuca()
 * 				OpinionDeInsecto: name() 
 */