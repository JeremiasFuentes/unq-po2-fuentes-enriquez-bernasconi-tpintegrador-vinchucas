package proyecto.muestras.busqueda;

import java.util.List;

import proyecto.muestras.Muestra;

//Patron: Composite. (Component)
public interface CriterioBusquedaMuestras {
	public List<Muestra> cumpleElCriterio(List<Muestra> listaDeMuestras);
}
