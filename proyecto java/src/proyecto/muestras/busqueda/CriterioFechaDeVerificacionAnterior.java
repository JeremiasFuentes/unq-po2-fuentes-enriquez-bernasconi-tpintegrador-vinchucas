package proyecto.muestras.busqueda;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import proyecto.muestras.Muestra;
//Patron: Composite. (Leaf)
public class CriterioFechaDeVerificacionAnterior implements CriterioBusquedaMuestras{
	//Para cumplir este criterio, las muestras tienen que haber sido verificadas despues de una cierta fecha recibida por parámetro
	private LocalDate fechaAComparar;

	public CriterioFechaDeVerificacionAnterior(LocalDate fechaAComparar) {
		super();
		this.fechaAComparar = fechaAComparar;
	}

	@Override
	public List<Muestra> cumpleElCriterio(List<Muestra> listaDeMuestras) {
		
		List <Muestra> muestrasQueCumplen = new ArrayList<Muestra>();
		for (Muestra m : listaDeMuestras) {
			if(!m.fueOpinadaDespuesDe(this.fechaAComparar)) {
				muestrasQueCumplen.add(m);
			}
		}
		return muestrasQueCumplen;
	}	
}
/*
 * Dependencias: 
 * 				Muestra: fueOpinadaDespuesDe(unaFecha)
 */