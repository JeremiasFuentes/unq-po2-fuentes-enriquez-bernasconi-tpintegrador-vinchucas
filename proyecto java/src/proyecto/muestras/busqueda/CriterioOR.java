package proyecto.muestras.busqueda;

import java.util.List;
import proyecto.muestras.Muestra;
//Patron: Composite. (Composite)
public class CriterioOR implements CriterioBusquedaMuestras{
	private CriterioBusquedaMuestras unCriterio;
	private CriterioBusquedaMuestras otroCriterio;
	
	public CriterioOR(CriterioBusquedaMuestras unCriterio, CriterioBusquedaMuestras otroCriterio) {
		super();
		this.unCriterio = unCriterio;
		this.otroCriterio = otroCriterio;
	}

	@Override
	public List <Muestra> cumpleElCriterio(List<Muestra> listaDeMuestras) {
		//Genero una lista con los que cumplen cada criterio
		List <Muestra> cumpleUno = unCriterio.cumpleElCriterio(listaDeMuestras);
		List <Muestra> cumpleOtro = otroCriterio.cumpleElCriterio(listaDeMuestras);
		//Quito de la segunda los que est�n repetidos de la primera
		cumpleOtro.removeAll(cumpleUno);
		//Luego junto ambas listas. No tendr� repetidos
		cumpleOtro.addAll(cumpleUno);
		/*
		 * //Agrego a la segunda los que est�n en la primera si es que no los tiene ya
		for (Muestra m : cumpleUno) {
			if(!cumpleOtro.contains(m)) {
				cumpleOtro.add(m);
			}
		}*/
		return cumpleOtro;
	}

}
