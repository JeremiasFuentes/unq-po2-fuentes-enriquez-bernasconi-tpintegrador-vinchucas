package proyecto.muestras;

import java.util.ArrayList;
import java.util.List;

public class Ubicacion {
	/*
	 * Esta clase representa la ubicaci�n geogr�fica, dada por dos valores en radianes
	 * llamados latitud y longitud, que representan al sistema de coordenadas referenciadas
	 * por la intersecci�n de un paralelo(direccion norte+ o sur- desde el Ecuador)
	 * y un meridiano(direccion este+ u oeste- desde el meridiano de Greenwich)
	 * */
	
	private double latitude,longitude;

	public Ubicacion(double latitude, double longitude) {
		super();
		this.setLatitude(latitude);
		this.setLongitude(longitude);
	}

	public double getLatitude() {
		return latitude;
	}

	private void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	private void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double distanceTo (Ubicacion destino) {
		//c�lculo de la distancia entre esta instancia y otra que se recibe como colaborador
		//ambas dadas por este sist de coordenadas
		double lat1=this.getLatitude();
		double lng1=this.getLongitude();
		double lat2=destino.getLatitude();
		double lng2=destino.getLongitude();
		
		double radioTierra = 6371;//en kil�metros  
        double dLat = Math.toRadians(lat2 - lat1);  
        double dLng = Math.toRadians(lng2 - lng1);  
        double sindLat = Math.sin(dLat / 2);  
        double sindLng = Math.sin(dLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distancia = radioTierra * va2;  
   
        return distancia;  
	}
	
	public ArrayList<Ubicacion> masCercanasQue(double distanciaMax, ArrayList<Ubicacion> ubicaciones){
		/*Dada una lista de ubicaciones y una distancia maxima ,
		 * retorna otra lista con las ubicaciones que est�n a distanciaMax
		 * o menor, de esta ubicacion */
		ArrayList<Ubicacion> cercanas = new ArrayList<Ubicacion>();
		for(Ubicacion ub: ubicaciones) {
			if (this.distanceTo(ub)<distanciaMax) { 
				cercanas.add(ub);
			}
		}
		return cercanas;
	}	
	
	public List <Muestra> obtenerMuestrasCercanas(double distanciaMax, Muestra muestra){
		/*Dada una lista de muestras y una distancia maxima ,
		 * retorna otra lista con las muestras de la lista que est�n a distanciaMax
		 * o menor de esta ubicacion.*/
		ArrayList <Muestra> muestrasCercanas = new ArrayList<Muestra>();
		ArrayList <Muestra> muestras = muestra.getSistema().getMuestras();
		for(Muestra m : muestras) {
			if (muestra.getUbicacion().distanceTo(m.getUbicacion()) <= distanciaMax) { 
				muestrasCercanas.add(m);
			}
		}
		return muestrasCercanas;
		
	}
}
