package proyecto.muestras;

import java.time.LocalDate;

public interface NivelDeUsuario { //Patron State: State
	public Boolean esExperto();
	public Opinion nuevaOpinion(OpinionDeInsecto insecto, LocalDate fecha);
}
