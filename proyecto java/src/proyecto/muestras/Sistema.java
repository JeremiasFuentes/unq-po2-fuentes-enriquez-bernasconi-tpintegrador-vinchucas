package proyecto.muestras;

import java.util.ArrayList;

public class Sistema {
	
	private ArrayList<ZonaDeCobertura> zonasDeCobertura;
	private ArrayList<Muestra> muestras;
	
	public Sistema() {
		this.zonasDeCobertura = new ArrayList<ZonaDeCobertura>();
		this.muestras = new ArrayList<Muestra>();
	}
	
	public ArrayList<ZonaDeCobertura> getZonasDeCobertura() {return zonasDeCobertura;}

	public ArrayList<Muestra> getMuestras() {return muestras;}

	
	public void agregarZona(ZonaDeCobertura zona) {
		this.zonasDeCobertura.add(zona);
	}
	
	public void sacarZona(ZonaDeCobertura zona) {
		this.zonasDeCobertura.remove(zona);
	}
	
	public void agregarMuestra(Muestra muestra) {
		this.muestras.add(muestra);
	}
	
	public void sacarMuestra(Muestra muestra) {
		this.muestras.remove(muestra);
	}

	public void getNotifyCreacionMuestra(Muestra unaMuestra) {
		this.agregarMuestra(unaMuestra);
		for(ZonaDeCobertura z:this.zonasDeCobertura) {
			if(z.contieneALaMuestra(unaMuestra)) {
				this.notifyZonaDeCoberturaNuevaMuestra(z, unaMuestra);
			}
		}	
	}
	
	public void getNotifyVerificacionMuestra(Muestra unaMuestra) {
		for(ZonaDeCobertura z:this.zonasDeCobertura) {
			if(z.contieneALaMuestra(unaMuestra)) {
				this.notifyZonaDeCoberturaValidacionMuestra(z, unaMuestra);
			}
		}	
	}

	private void notifyZonaDeCoberturaNuevaMuestra(ZonaDeCobertura zona, Muestra unaMuestra) {
		zona.notificarNuevaMuestra(unaMuestra);
	}
	
	private void notifyZonaDeCoberturaValidacionMuestra(ZonaDeCobertura zona, Muestra unaMuestra) {
		zona.notificarNuevaValidacion(unaMuestra);
	}
}
