package proyecto.muestras;

public enum TipoOrganizacion {

		SALUD("Salud"),
		EDUCATIVA("Educativa"),
		CULTURAL("Cultural"),
		ASISTENCIA("Asistencia"); 
		
		private String tipoOrganizacion;
		
		TipoOrganizacion(String tipo) {
			this.setTipoOrganizacion(tipo);			
		}

		public String getTipoOrganizacion() {return tipoOrganizacion;}

		public void setTipoOrganizacion(String tipoOrganizacion) {this.tipoOrganizacion = tipoOrganizacion;}
		
}
