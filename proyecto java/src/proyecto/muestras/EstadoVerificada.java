package proyecto.muestras;

import java.util.HashMap;
import java.util.List;

public class EstadoVerificada extends EstadoDeVerificacion{

	public EstadoVerificada() {
		this.nivelDeValidacion = NivelDeValidacion.Verificada;
	}
	
	@Override
	protected void addOpinion(Opinion opinion, Verificacion verificacion) throws Exception {
		throw new Exception("No es posible votar, Muestra ya verificada");
	}
	
	@Override
	protected boolean esImportante(Opinion op) {
		return op.esDeExperto();
	}

	@Override
	protected HashMap<String, Integer> countRepetitions(List<Opinion> votaciones) {
		return null;
	}

	@Override
	protected int contarOpinion(Opinion op) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected void agregarAlContador(HashMap<String, Integer> counter, Opinion op) {
		// TODO Auto-generated method stub
		
	}

}
