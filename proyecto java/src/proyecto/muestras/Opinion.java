package proyecto.muestras;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

public abstract class Opinion {

	protected OpinionDeInsecto opinionDeInsecto;
	protected LocalDate fecha;

	public String veredicto() {
		return this.opinionDeInsecto.getNombreClasificacion();
	}

	abstract public boolean esDeExperto();

	
	//Las opiniones tienen que tener fecha
	public Boolean esReciente() {
		Long antiguedad = ChronoUnit.DAYS.between(this.getFecha(), LocalDate.now());
		return (antiguedad<=30);
	}

	public LocalDate getFecha() {
		return this.fecha;
	}

	protected abstract void opinarVotada(Verificacion verificacion);

	protected abstract void opinarVotadaExperto(Verificacion verificacion) throws Exception;

	protected abstract int contarParaVerificacion(String veredicto);

	protected Integer sumarAlContadorExperto() {
		// TODO Auto-generated method stub
		return null;
	}

	protected void contarVotoExperto(HashMap<String, Integer> map) {
		// TODO Auto-generated method stub
		
	}

}
