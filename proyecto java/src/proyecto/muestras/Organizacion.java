package proyecto.muestras;

public class Organizacion {
	
	private TipoOrganizacion tipoOrganizacion;
	private int cantidadPersonas;
	private FuncionalidadExterna funcionalidadNuevaMuestra;
	private FuncionalidadExterna funcionalidadNuevaValidacion;

	public Organizacion(TipoOrganizacion tipoO, int cantidad) {
		this.setCantidadPersonas(cantidad);
		this.setTipoOrganizacion(tipoO);
	}

	public TipoOrganizacion getTipoOrganizacion() {	return tipoOrganizacion;}
	public void setTipoOrganizacion(TipoOrganizacion tipoOrganizacion) {this.tipoOrganizacion = tipoOrganizacion;}

	public int getCantidadPersonas() {return cantidadPersonas;}
	public void setCantidadPersonas(int cantidadPersonas) {this.cantidadPersonas = cantidadPersonas;}
	
	public void setFuncionalidadNuevaMuestra(FuncionalidadExterna funcionalidad) {
		this.funcionalidadNuevaMuestra = funcionalidad;
	}
	
	public FuncionalidadExterna getFuncionalidadNuevaMuestra() {
		return funcionalidadNuevaMuestra;
	}
	
	public FuncionalidadExterna getFuncionalidadNuevaValidacion() {
		return funcionalidadNuevaValidacion;
	}
	
	public void setFuncionalidadNuevaValidacion(FuncionalidadExterna funcionalidad) {
		this.funcionalidadNuevaValidacion = funcionalidad;
	}

	public void updateNuevaValidacion(ZonaDeCobertura zonaDeCobertura, Muestra muestra){
		this.getFuncionalidadNuevaValidacion().nuevoEvento(this, zonaDeCobertura, muestra);
	}
	
	public void updateNuevaMuestra(ZonaDeCobertura zonaDeCobertura, Muestra muestra){
		this.getFuncionalidadNuevaMuestra().nuevoEvento(this, zonaDeCobertura, muestra);
	}
}
