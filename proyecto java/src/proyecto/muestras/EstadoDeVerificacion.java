package proyecto.muestras;

import java.util.HashMap;
import java.util.List;

public abstract class EstadoDeVerificacion {
	
	protected NivelDeValidacion nivelDeValidacion;
	
	public EstadoDeVerificacion() {
		this.nivelDeValidacion = NivelDeValidacion.Votada;
	}

	protected abstract void addOpinion(Opinion opinion, Verificacion verificacion) throws Exception;

	public String nivelDeValidacion() {
		//todas devuelven esto excepto el estadoVerificada, al estadoNoVotada nunca deberia recibir este mensaje ya que siempre
		//la muestra comienza con un voto.
		return this.nivelDeValidacion.getTipoValidacion();
	}	

	protected abstract boolean esImportante(Opinion op);
	
	protected abstract int contarOpinion(Opinion op);

	protected abstract void agregarAlContador(HashMap<String, Integer> counter, Opinion op);
	
	protected HashMap<String, Integer> countRepetitions(List<Opinion> votaciones){
		HashMap<String, Integer> counter = new HashMap<String, Integer>();
		counter.put("No Definida", 0);
		for (Opinion op : votaciones) {
			String opinionInsecto = op.veredicto();
			if (counter.containsKey(opinionInsecto)) {
				int contadorActualizado = counter.get(opinionInsecto) + this.contarOpinion(op);
				counter.put(opinionInsecto, contadorActualizado);
			} else {
				agregarAlContador(counter, op);
			}
		}	
		return counter;
	}
}
