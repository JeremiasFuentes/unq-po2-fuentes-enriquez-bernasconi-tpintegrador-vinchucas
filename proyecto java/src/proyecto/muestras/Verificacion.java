package proyecto.muestras;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Verificacion {
	
	private List<Opinion> votaciones = new ArrayList<Opinion>();
	private EstadoDeVerificacion estado = new EstadoNoVotada();
	private String opinionMasVotada = "No Definida";
	private LocalDate fechaUltimaOpinion;
	private Muestra muestra;
	
	public Verificacion(Muestra muestra) {
		this.muestra = muestra;
	}
	
	public void agregarOpinion (Opinion op) {
		this.listaDeOpiniones().add(op);
	}

	public Muestra getMuestra() {
		return this.muestra;
	}
	
	public void addOpinion(Opinion opinion) throws Exception {
		//se le pasa la propia verificacion para que el estado pueda decidir a que estado se debe cambiar luego de agregar la votacion.
		estado.addOpinion(opinion,this);
	}

	public String getOpinionMasVotada() {
		return this.opinionMasVotada;		
	}
	
	private void setOpinionMasVotada(HashMap<String, Integer> counter) {
		String opinionMasVotada = this.opinionMasVotada;
		int cantidadDeVotosOpinionMasVotada = counter.get(this.opinionMasVotada);
		
		Set<String> setSinOpinionMasVotada = counter.keySet();
		setSinOpinionMasVotada.remove(opinionMasVotada);
		
		for(String k : setSinOpinionMasVotada) {
			int cantidadVotosOpinionAComparar = counter.get(k);
			if(cantidadVotosOpinionAComparar > cantidadDeVotosOpinionMasVotada) {
				opinionMasVotada = k;
				cantidadDeVotosOpinionMasVotada = cantidadVotosOpinionAComparar;
			} else if(cantidadVotosOpinionAComparar == cantidadDeVotosOpinionMasVotada) {
				opinionMasVotada = "No Definida";	
				cantidadDeVotosOpinionMasVotada = cantidadVotosOpinionAComparar;
			}
		}	
		this.opinionMasVotada = opinionMasVotada;
	}

	public List<Opinion> listaDeOpiniones() {
		return this.votaciones;
	}

	public String nivelDeValidacion() {
		return estado.nivelDeValidacion();
	}

	protected void setEstado(EstadoDeVerificacion estadoNuevo) {
		this.estado = estadoNuevo;
	}
	
	/* CODIGO ANTERIOR DE CALCULAR OPINION CON MAS VOTOS
	// para ver si una verificacion es no definida, se fija si hay alg�n empate de votos con la opinion de m�s votos actual.
	protected boolean noEstaDefinida(HashMap<String, Integer> counter) {
		int cantEnEmpate = 0;
		for(Opinion op : this.listaDeOpiniones()) {
			if(this.esCompetencia(op, this.opinionMasVotada())) {
				cantEnEmpate = cantEnEmpate + 1;
			}
		}
		return cantEnEmpate >= 1;
	}
	
	// una opinion es competencia de otra si tienen la misma cantidad de votos importantes y sus veredictos son diferentes.
	protected boolean esCompetencia(Opinion op, Opinion opinion) {
		return ((this.votosImportantesDe(op) == this.votosImportantesDe(opinion)) && !(op.veredicto().equals(opinion.veredicto())));
	}

	protected Opinion opinionMasVotada() {
		Opinion p = this.votaciones.get(0);
		for(Opinion op : this.votaciones) {
			if(this.votosImportantesDe(op) >= this.votosImportantesDe(p)) {
				p = op;
			}
		}
		return p;
	}
	
	// los votos importantes dependen del estado, pueden importarles que las opiniones sean expertas o no.
	protected int votosImportantesDe(Opinion opinion) {
		int cantVotos = 0;
		for(Opinion p2 : this.listaDeOpiniones()) {
			if ((opinion.veredicto().equals(p2.veredicto())) && estado.esImportante(p2)) {
				cantVotos = cantVotos + 1;
			}
		}
		return cantVotos;
	}
*/	
	
	public LocalDate getFechaUltimaOpinion() {
		return this.fechaUltimaOpinion;
	}

	public void setFechaUltimaOpinion(LocalDate fecha) {
		this.fechaUltimaOpinion = fecha;
	}

	public void comprobarSiEstaVerificada() {
		if(this.estaVerificada()) {
			this.setEstado(new EstadoVerificada());
			this.getMuestra().notifySistemaVerificacionMuestra();
		}
	}
	
	private Boolean estaVerificada() {
		int votacionesDeExperto = 0;
		for(Opinion op : this.listaDeOpiniones()) {
			votacionesDeExperto = votacionesDeExperto + op.contarParaVerificacion(this.getOpinionMasVotada());
		}
		return votacionesDeExperto >= 2;
	}

	public void agregarOpinionALista(Opinion opinion) {
		this.listaDeOpiniones().add(opinion);
		HashMap<String, Integer> counter = this.estado.countRepetitions(this.listaDeOpiniones());
		this.setOpinionMasVotada(counter);
	}

	public void cambioDeEstadoVotadaAVotadaPorExperto(EstadoVotadaPorExperto estadoVotadaPorExperto) {
		this.setEstado(estadoVotadaPorExperto);
		this.opinionMasVotada = "No Definida";
	}
}
