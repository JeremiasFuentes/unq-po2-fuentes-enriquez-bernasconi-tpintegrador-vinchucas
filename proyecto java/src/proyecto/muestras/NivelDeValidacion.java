package proyecto.muestras;

public enum NivelDeValidacion {

	Votada("Votada"),
	Verificada("Verificada"); 
	
	private String tipoValidacion;
	
	NivelDeValidacion(String tipo) {
		this.setTipoValidacion(tipo);			
	}

	public String getTipoValidacion() {
		return tipoValidacion;
	}

	public void setTipoValidacion(String tipoValidacion) {
		this.tipoValidacion = tipoValidacion;
	}

}