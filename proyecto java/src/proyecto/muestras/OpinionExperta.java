package proyecto.muestras;

import java.time.LocalDate;
import java.util.HashMap;

public class OpinionExperta extends Opinion{

	public OpinionExperta(OpinionDeInsecto insecto, LocalDate fecha) {
		this.opinionDeInsecto = insecto;
		this.fecha = fecha;
	}

	@Override
	public boolean esDeExperto() {
		return true;
	}

	@Override
	protected void opinarVotada(Verificacion verificacion) {
		verificacion.cambioDeEstadoVotadaAVotadaPorExperto(new EstadoVotadaPorExperto());
		verificacion.agregarOpinionALista(this);
		verificacion.setFechaUltimaOpinion(this.getFecha());
	}
	
	@Override
	protected void opinarVotadaExperto(Verificacion verificacion) {
		verificacion.agregarOpinionALista(this);
		verificacion.comprobarSiEstaVerificada();
		verificacion.setFechaUltimaOpinion(this.getFecha());
	}

	@Override
	protected int contarParaVerificacion(String veredicto) {
		int contable = 0;
		if(this.veredicto() == veredicto) {
			contable = 1;
		}
		return contable;
	}

	@Override
	protected void contarVotoExperto(HashMap<String, Integer> map) {
		map.put(this.veredicto(), 1);
	}

	@Override
	protected Integer sumarAlContadorExperto() {
		return 1;
	}

}
